FROM powerai/tensorflow-gpu:2.1.0
RUN mkdir -p /var/mnist-example
WORKDIR /var/mnist-example
COPY ./ /var/mnist-example
CMD ["python3","script2.py"]